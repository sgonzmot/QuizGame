sap.ui.define(
	["com/santiago/QuizAdminFront/controller/BaseController", "sap/ui/model/json/JSONModel"],
	function (Controller, JSONModel) {
		"use strict";

		return Controller.extend("com.santiago.QuizAdminFront.controller.Main", {
			onInit: async function () {
				let result = await fetch("/admin/api/Users");

				let data = JSON.parse(await result.text());

				var oModel = new JSONModel({ UsersCollection: data.value });
				this.getView().setModel(oModel);
			},
			delete: async function () {
				let name = this.getView().byId("inputName").getValue();
				let requestOptions = {
					method: "DELETE",
				};
				await fetch(`/admin/api/Users('${name}')`, requestOptions);

				this.onInit();
			},
			save: async function () {
				let name = this.getView().byId("inputName").getValue();
				let password = CryptoJS.AES.encrypt(
					this.getView().byId("inputPassword").getValue(),
					"secret key 123"
				).toString();
				let admin = this.getView().byId("checkAdmin").getSelected();
				let requestOptions, myHeaders;

				myHeaders = new Headers();
				myHeaders.append("Content-Type", "application/json");
				let raw = JSON.stringify({
					name: name,
					password: password,
					privileges: admin,
				});
				requestOptions = {
					method: "POST",
					headers: myHeaders,
					body: raw,
				};

				await fetch("/admin/api/Users", requestOptions);
				this.onInit();
			},
		});
	}
);
