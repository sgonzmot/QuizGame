/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function() {
  "use strict";

  sap.ui.require([
    "com/santiago/QuizAdminFront/test/integration/AllJourneys"
  ], function() {
    QUnit.start();
  });
});
