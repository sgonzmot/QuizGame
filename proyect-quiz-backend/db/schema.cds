using {cuid} from '@sap/cds/common';

namespace db;

entity Question {
    key id          : Integer;
        question    : String;
        answers     : Composition of many Answer
                          on answers.question = $self;
        rewards     : Composition of many Reward
                          on rewards.question = $self;
        punishments : Composition of many Punishment
                          on punishments.question = $self;
}

entity Reward : cuid {
    data     : String;
    question : Association to Question;
}

entity Punishment : cuid {
    data     : String;
    question : Association to Question;
}

entity Users {
    key name       : String;
        password   : String;
        privileges : Boolean;
}

entity Answer : cuid {
    answer   : String;
    question : Association to Question;
}
