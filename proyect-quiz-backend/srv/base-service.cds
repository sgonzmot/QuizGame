using {db} from '../db/schema';

@(path : '/base/api')
service Base {

    function getLogin(name : String, password : String) returns Boolean;
}
