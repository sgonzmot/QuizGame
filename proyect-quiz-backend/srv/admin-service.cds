using {db} from '../db/schema';

@(path : '/admin/api')
@requires : 'authenticated'
service Admin {

    entity Question   as projection on db.Question;
    entity Answer     as projection on db.Answer;
    entity Punishment as projection on db.Punishment;
    entity Reward     as projection on db.Reward;
    entity Users      as projection on db.Users;

}
