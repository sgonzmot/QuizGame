const cds = require('@sap/cds')
const CryptoJS = require("crypto-js");

module.exports = async (srv) => {
  const db = await cds.connect.to ('db')
  const { Users } = db.entities ('db')

  srv.before ('CREATE','Users', each => {

      let ciphertext = CryptoJS.AES.encrypt(each.data.password, 'secret key 123').toString();
      
      each.data.password = ciphertext;
  })

  srv.after ('READ','Users', each => {
    

  })

}
